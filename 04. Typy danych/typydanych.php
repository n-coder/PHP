<?php
//w PHP występuje 8 podstawowych typów danych

/*
1. Skalarne
    a. Boolean - true/false
    b. integer - -123, 123 
    c. float - 12.3 , 3.89898
    d. string - 'napis', "napis", heredoc
2. Złożone
    a. array - typ tablicowy
    b. object - typ obiektowy
3. Specjalne
    a. resouece - odwołanie do zasobu np. połączenie z baą danych
    b. null - brak wartośći
*/

// przykłady zmiennej typu boolean
$boolean1 = true;
$boolean2 = false;

//integer
$integer1 = 123;
$integer2 = -123;

//float
$float1 = 3.5;
$float2 = -3.5;
$float3 = 2e3;
$float4 = -4.7e-2;
$float5 = 5.4^-2;

//inne formaty przedstawiania typów danych
$format10 = 123; //cyfry z zakresu 0-9
$format8 = 013; //cyfry z zakresu 0-7
$format16 = 0x0F0; //cyfry i litery od 0 do F;


?>