<?php

/*
Pętla while

while(warunek){
    instrukcje
}
*/

$age = 3;

while ($age <= 18){
    echo "$age <br>";
    $age++;
}
echo "<h3>przykład z tablicą:</h3><br>";
$tab = [1,2,4, "hej", 4.4];
$i = 0;
while ($i<count($tab)){
    echo "$tab[$i]<br>";
    $i++;
}

echo "<h3>do ... while <br></h3>";

$i = 0;
do {
    echo "$tab[$i]<br>";
    $i++;
} while ($i<count($tab)); 