<?php
    $imie = "Jaś";
    $nazwisko = "Kowalski";
    $wiek = 20;
    
    printf ('Mam na imie %s. Moje nazwisko to %s i mam %d lat.<br>', $imie, $nazwisko, $wiek);

    /*
    %s - wyswietlanie tekstu (string),
    %d - wyswietlanie liczb całkowitych (decimal)   5, 4, 6, itp
    %f - wyswietlanie liczby zmiennoprzecinkowej (float)   4.5, 234.1, - 12,4
    */

    //Przykład
    
    $dzielna = 4;
    $dzielnik = 3;
    $iloraz = $dzielna/$dzielnik;

    //zwraca liczbę całkowitą
    printf("Wynikiem dzielenia %d przez %d jest %d", $dzielna, $dzielnik, $iloraz); 
    echo "<br>";
    //zwraca liczbę zmiennoprzeciokową
    printf("Wynikiem dzielenia %d przez %d jest %f", $dzielna, $dzielnik, $iloraz); 
    echo "<br>";
    //zwraca liczbę zmiennoprzecinkową z dokladnością do 3 miejsc po przecinku
    printf("Wynikiem dzielenia %d przez %d jest %.3f", $dzielna, $dzielnik, $iloraz); 
?>