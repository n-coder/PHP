<?php

/*
tworzymy tablicę
 */
$tab = array();
/*
wyświetlamy tablicę iteracyjną
 */
print_r($tab); //wyświetla strukturę tablicy
echo "<br>";

$tab = array(1,2,3, 34=>"LIN", "ŁANCUCH");
print_r($tab);

echo "<br>";
$tab1[23] = 3;
$tab1[404] = "string";
print_r($tab1);

echo "<br>";
/* wyśwyświetlamy elementy tablicy
*/
for ($i=0; $i<count($tab); $i++) {
    echo $tab[$i]."<br>";
}
echo "<br>";
//while
$i = 0;
while($i<count($tab)) {
    echo $tab[$i].", ";
    $i++;
}
//foreach
$tab = array(1,2,3, "jeden"=>"LIN", "ŁANCUCH");
echo "<br>";
foreach ($tab as $key => $value) {
    echo "$key - $value <br>";
}

//*inny while

reset($tab); //resetujemy wskaznik na zerowy index tablicy(petla foreach przesunela nam go na koniec tablicy)
while(list($key,$value) = each($tab)) {
    echo $value.", ";
}
echo "<br>";

//jeszcze inny while
reset($tab);
while (true) {
    echo current($tab). ', ';
    if(next($tab) === false){
        break;
    }
}
echo "<br>";

//Wyświetlanie elementu asocjacyjnego
echo "To jest element asocjacyjny: {$tab['jeden']} <br>";

/*tablice asocjacyjne
KLUCZAMI (INDEKSAMI) nie są liczby.
*/
$tablica = array(
    "jeden" => 1,
    "dwa" => 2
);

foreach ($tablica as $k=>$t) {
    echo $k - $t . "<br>";
}
?>
