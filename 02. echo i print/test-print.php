<?php
//Test prędkości Print
print "<h2>Test prędkości PRINTO<br></h2>";

$iloscPowtorzen = 10;
$suma = 0;
for($i = 0; $i < $iloscPowtorzen; $i++)
{
    $start = microtime(true);
    $j =0;

    while ($j++ < 1000)
    {
        print "Test prędkości PRINT<br>";
    }

    $stop = microtime(true);
    $suma += $stop - $start;
}
print "Średnio pętla przy użyciu \"print\" wykonywała się przez: " . ($suma/$iloscPowtorzen) . " sekund";
?>