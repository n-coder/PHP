<?php
//Test prędkości Echo
echo "<h2>Test prędkości ECHO<br></h2>";

$iloscPowtorzen = 10;
$suma = 0;
for($i = 0; $i < $iloscPowtorzen; $i++)
{
    $start = microtime(true);
    $j =0;

    while ($j++ < 1000)
    {
        echo "Test prędkości ECHO<br>";
    }

    $stop = microtime(true);
    $suma += $stop - $start;
}
echo "Srednio pętla przy użyciu \"echo\" wykonywała się przez: " . ($suma/$iloscPowtorzen) . " sekund";
?>