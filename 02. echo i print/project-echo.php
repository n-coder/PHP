<?php
//echo
$zmienna = 1;
$zmiennaa = 3;
echo 'To jest pierwszy tekst wyświetlony za pomocą "echo"! <br>';
echo $zmienna, $zmiennaa; //tak zadziała :)
//print
print 'To jest text wyświetlony za pomocą "print"!<br>';
/*
print $zmienna, $zmiennaa;  

!!!!tak to nie zadziała :)

"print" może przyjąć tylko jedną wartość
*/

//różnice pomiędzy apostrofami a cudzysłowami.
echo 'To jest $zmienna!<br>'; //wysietli się nazwa zmiennej
echo "To jest $zmienna ! <br>"; //wyświetli się wartość zmiennej

//konkatenacja
$zmienna1 = "napis";
$zmienna2 = "drugi";
echo $zmienna2 . " " . "$zmienna1<br>" ;


//heredoc

$zmiennaHeredoc = <<< TEXT
To jest napis heredoc, możemy wstawić tutaj dowolny tekst.

TEXT;
echo "$zmiennaHeredoc<br>";
//ważne aby zamknięcie było ustawione przy lewej krawędzi w przeciwnym wypadku wystąpią 
?>