<?php
/*
Wykrywanie typu zmiennej gettype(nzawaZmiennej)
*/

$zmienna = 5;
echo gettype($zmienna);
echo "<br>";

$zmienna = 5.7;
echo gettype($zmienna);
echo "<br>";

$zmienna = 5e3;
echo gettype($zmienna);
echo "<br>";

$zmienna = "Imię";
echo gettype($zmienna);
echo "<br>";

$zmienna = [];
echo gettype($zmienna);
echo "<br>";

$zmienna = array();
echo gettype($zmienna);
echo "<br>";

/*
Sprawdanie okreslonego typy zmiennej

is_array(nazwaZmennej);
is_bool();
is_float();
is_int();
is_null();
is_object();
is_resource();
is_string();
*/

$zmiennaInt = 1;

if (is_int($zmiennaInt))
{
    echo 'Zmienna $zmiennaInt jest typu integer <br>';
} 
else 
{
    echo 'zmienna $zmiennaInt jest jednak innego typu<br>';
}

$tabliczka = [1,2,3,4,5,7];
if(is_array($tabliczka))
{
    echo 'Zmienna $tabliczka jest typu tablicowego';
}

/*
Zmienne globalne
$_GLOBALS
$_SERVER przechowuje informacje o serwerze
$_GET zawiera dane przekazywane do serwera
$_POST niejawne przekazywanie do serwera
$_COOKIE zapisujemy małe partie danych
$_FILES elementy przekazane do skryptu przez metode POST
$_ENV wartosci zmiennych srodowiskowych 
$_REQUEST dane ze zmiennych $_GET $_POST $_COOKIE
$_SESSION zawiera dane zwiazane z bieżacą sesja
*/

?>