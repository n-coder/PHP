<?php
/* 
= przypisanie
== równość argumentów
=== równość typów argumentów
<> lub != różne argumenty
!== różne typy argumentów
< mniejsze
> większe
<= mniejszy lub równy
>= większy lub równy

+ - * / % ^ .

Zapis tradycyjny
a = a + b
d = a % d

Zapis skrócony

a += b
d %= b
*/
$a = 3;
$b = 2;
echo $a += $b;
echo '<br>';
echo $a%$b;
echo '<br>';

$a %= $b;
echo $a;