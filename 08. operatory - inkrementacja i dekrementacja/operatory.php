<?php 
/*
Pre inkrementacja ++$i
Post inkrementacja $i++
Pre dekrementacja --$i
Post dekrementacja $i--

 */

$a = 3;
echo ++$a;
echo '<br>';
$a = 3;
echo $a++;
echo '<br>';
$a = 3;
echo --$a;
echo '<br>';
$a = 3;
echo $a--;
echo '<br>';
?>