<?
/*
1.if
2.if else
3.elseif
4. zagnieżdżone if (else)
*/

/* rownanie kwadratowe */

$wiek = 10;

if ($wiek >= 15) {
    echo "Ola idzie do kina"; 
} else {
    echo "Ola jest za młoda żeby iść na ten film";
}

echo '<br>';

if ($wiek >= 10) {
    echo "Ola idzie do kina"; 
} elseif ($wiek>=10) {
    echo "Ola może iść do szkoy";
} else {
    echo "Ola jest za młoda żeby iść gdziekolwiek";
}

echo '<br>';

$wiek = 9;

if ($wiek >= 10) {
    echo "Ola idzie do kina"; 
} else {
    if ($wiek>=10){
        echo "Ola może iść do szkoy";
    } else {
        echo "Ola jest za młoda żeby iść gdziekolwiek";
    }
} 
