<?php
/* 
Co to jest zmienna? 
    Kontener w którym przechowujemy wartości

Zasady nazywania zmiennych: 
    $zmienna, $Zmienna, $ZMIENNa to ni ete same zmienne, na początku nie może wystąpić cyfra.

Czy typ zmiennej jest stały?
    Nie jest stały. możemy zmieniac typ danych w "locie"

W Php występuje niejawny typ danych.
*/

$nazwaZmiennej = 1; //int
echo $nazwaZmiennej;
echo "<br>";
$nazwaZmiennej = 1.5; //float
echo $nazwaZmiennej;
echo "<br>";
$nazwaZmiennej = "nazwa"; //string
echo $nazwaZmiennej;


?>